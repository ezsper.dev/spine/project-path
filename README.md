# Spine Project Path Utility

It's an utility module to determines project root path

### How to use it

```
import { getProjectPath } from '@spine/project-path';
import { exec } from 'child_process';

exec('npm install', {
  stdio: 'inherit',
  cwd: getProjectPath(),
});
```

### Matching order

To determine project path it follows these strict cases in order...

##### First case scenario

If `process.cwd()` contains a `/node_modules/` return the path before it;

##### Second case scenario

If a `package.json` is present in `process.cwd()` path, than returns the same path;

##### Third case scenario

If `@spine/project-path` installation path contains `/node_modules/` return the path before it;

> If `@spine/project-path` is a submodule of another installed dependency, it will return the parent
of all, which means the parent directory of the first encounter of `/node_modules/`.

##### Default scenario

Otherwise throws an error.

If you don't want any possibility for an error you can use

```
setProjectDefaultPath(() => {
  return '-- Your default project root path fallback --';
});
``` 