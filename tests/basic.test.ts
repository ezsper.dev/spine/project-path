/// <reference types="jest" />
import * as path from 'path';
import { getProjectPath, setProjectPath } from '../index';

describe('Basic', () => {
  it('Set Project Path', async () => {
    setProjectPath(__dirname);
    expect(getProjectPath()).toBe(__dirname);
  });
  it('Get Project Path', async () => {
    setProjectPath(undefined);
    expect(getProjectPath()).toBe(path.resolve(__dirname, '..'));
  });
});

