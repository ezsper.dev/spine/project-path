export declare let projectPath: string | undefined;
export declare let projectDefaultPath: string | undefined | (() => string | undefined);
export declare function getProjectPath(): string;
export declare function setProjectPath(value: string | undefined): void;
export declare function setProjectDefaultPath(value: string | undefined | (() => string | undefined)): void;
