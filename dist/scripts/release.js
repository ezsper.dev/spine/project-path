"use strict";

var _interopRequireWildcard = require("@babel/runtime-corejs3/helpers/interopRequireWildcard");

require("core-js/modules/es.array.join");

require("core-js/modules/es.regexp.exec");

var _ = require("..");

var _path = require("path");

var fs = _interopRequireWildcard(require("fs-extra"));

var _exec = require("../internal/utils/exec");

/**
 * This script builds to distribuition
 */
// First clear the build output dir.
fs.removeSync((0, _path.resolve)((0, _.getProjectPath)(), './dist')); // Ignore all that is not used on client

var ignore = ["node_modules", "bin", "build", "dist", "tests", "types.d", "sample"].join(','); // Then compile to pure JS

(0, _exec.exec)("tsc --emitDeclarationOnly --skipLibCheck && babel --source-maps --ignore ".concat(ignore, " -d dist --extensions \".ts,.tsx\" .")); // Copy documents

fs.copySync((0, _path.resolve)((0, _.getProjectPath)(), 'README.md'), (0, _path.resolve)((0, _.getProjectPath)(), 'dist/README.md')); // Clear our package json for release

var packageJson = require('../package.json');

packageJson.main = './index.js';
delete packageJson.scripts;
delete packageJson.private;
delete packageJson['lint-staged'];
delete packageJson.jest;
delete packageJson.devDependencies;
fs.writeJsonSync((0, _path.resolve)((0, _.getProjectPath)(), './dist/package.json'), packageJson, {
  spaces: 2
});
fs.removeSync((0, _path.resolve)((0, _.getProjectPath)(), './dist/internal'));
fs.copySync((0, _path.resolve)((0, _.getProjectPath)(), 'LICENSE'), (0, _path.resolve)((0, _.getProjectPath)(), 'dist/LICENSE'));
//# sourceMappingURL=release.js.map