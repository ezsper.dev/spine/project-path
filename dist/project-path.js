"use strict";

var _interopRequireWildcard = require("@babel/runtime-corejs3/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

require("core-js/modules/es.array.concat");

require("core-js/modules/es.array.join");

require("core-js/modules/es.object.define-property");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.projectPath = exports.installPath = void 0;

var _indexOf = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/index-of"));

var fs = _interopRequireWildcard(require("fs"));

var path = _interopRequireWildcard(require("path"));

var installPath = __dirname;
exports.installPath = installPath;
var projectPath;
exports.projectPath = projectPath;
var NODE_MODULES = "".concat(path.sep, "node_modules").concat(path.sep);
var cwd = process.cwd();
var pos = (0, _indexOf.default)(cwd).call(cwd, NODE_MODULES);

if (pos >= 0) {
  exports.projectPath = projectPath = cwd.substring(0, pos);
} else if (fs.existsSync(path.join(cwd, 'package.json'))) {
  exports.projectPath = projectPath = cwd;
} else {
  pos = (0, _indexOf.default)(__dirname).call(__dirname, NODE_MODULES);

  if (pos >= 0) {
    exports.projectPath = projectPath = installPath.substring(0, pos);
  }
}
//# sourceMappingURL=project-path.js.map