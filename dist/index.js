"use strict";

require("core-js/modules/es.object.define-property");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getProjectPath = getProjectPath;
exports.setProjectPath = setProjectPath;
exports.setProjectDefaultPath = setProjectDefaultPath;
exports.projectDefaultPath = exports.projectPath = void 0;
var projectPath;
exports.projectPath = projectPath;
var projectDefaultPath;
exports.projectDefaultPath = projectDefaultPath;

function getProjectPath() {
  if (projectPath == null) {
    if (typeof process !== 'undefined') {
      if (process.env.BUILD_FLAG_IS_BROWSER !== 'true') {
        if (typeof window === 'undefined') {
          var _require = require('./project-path');

          exports.projectPath = projectPath = _require.projectPath;
        }
      }
    }
  }

  if (projectDefaultPath != null) {
    if (typeof projectDefaultPath === 'function') {
      var _projectPath = projectDefaultPath();

      if (_projectPath != null) {
        return _projectPath;
      }
    } else {
      return projectDefaultPath;
    }
  }

  if (projectPath == null) {
    throw new Error("Couldn't determine project path, you might need to set it manually");
  }

  return projectPath;
}

function setProjectPath(value) {
  exports.projectPath = projectPath = value;
}

function setProjectDefaultPath(value) {
  exports.projectDefaultPath = projectDefaultPath = value;
}
//# sourceMappingURL=index.js.map