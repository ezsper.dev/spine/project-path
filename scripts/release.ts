/**
 * This script builds to distribuition
 */
import { getProjectPath } from '..';
import { resolve as pathResolve } from 'path';
import * as fs from 'fs-extra';
import { exec } from '../internal/utils/exec';

// First clear the build output dir.
fs.removeSync(pathResolve(getProjectPath(), './dist'));

// Ignore all that is not used on client
const ignore = [
  "node_modules",
  "bin",
  "build",
  "dist",
  "tests",
  "types.d",
  "sample"
].join(',');

// Then compile to pure JS
exec(`tsc --emitDeclarationOnly --skipLibCheck && babel --source-maps --ignore ${ignore} -d dist --extensions \".ts,.tsx\" .`);

// Copy documents
fs.copySync(
  pathResolve(getProjectPath(), 'README.md'),
  pathResolve(getProjectPath(), 'dist/README.md'),
);
// Clear our package json for release
const packageJson = require('../package.json');
packageJson.main = './index.js';
delete packageJson.scripts;
delete packageJson.private;
delete packageJson['lint-staged'];
delete packageJson.jest;
delete packageJson.devDependencies;

fs.writeJsonSync(
  pathResolve(getProjectPath(), './dist/package.json'),
  packageJson,
  { spaces: 2 },
);

fs.removeSync(pathResolve(getProjectPath(), './dist/internal'));

fs.copySync(
  pathResolve(getProjectPath(), 'LICENSE'),
  pathResolve(getProjectPath(), 'dist/LICENSE'),
);
