import * as fs from 'fs';
import * as path from 'path';

export const installPath = __dirname;
export let projectPath: string | undefined;

const NODE_MODULES = `${path.sep}node_modules${path.sep}`;
const cwd = process.cwd();
let pos = cwd.indexOf(NODE_MODULES);

if (pos >= 0) {
  projectPath = cwd.substring(0, pos);
} else if (fs.existsSync(path.join(cwd, 'package.json'))) {
  projectPath = cwd;
} else {
  pos = __dirname.indexOf(NODE_MODULES);
  if (pos >= 0) {
    projectPath = installPath.substring(0, pos);
  }
}