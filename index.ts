export let projectPath: string | undefined;
export let projectDefaultPath: string | undefined | (() => string | undefined);

export function getProjectPath(): string {
  if (projectPath == null) {
    if (typeof process !== 'undefined') {
      if (process.env.BUILD_FLAG_IS_BROWSER !== 'true') {
        if (typeof window === 'undefined') {
          ({ projectPath } = require('./project-path'));
        }
      }
    }
  }

  if (projectDefaultPath != null) {
    if (typeof projectDefaultPath === 'function') {
      const projectPath = projectDefaultPath();
      if (projectPath != null) {
        return projectPath;
      }
    } else {
      return projectDefaultPath;
    }
  }

  if (projectPath == null) {
    throw new Error(`Couldn't determine project path, you might need to set it manually`);
  }

  return projectPath;
}

export function setProjectPath(value: string | undefined) {
  projectPath = value;
}

export function setProjectDefaultPath(value: string | undefined | (() => string | undefined)) {
  projectDefaultPath = value;
}